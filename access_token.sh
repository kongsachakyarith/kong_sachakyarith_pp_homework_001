#Bonus
#get an access token (a shell script will be provided by the instructor)
HOST="https://saintrivers.tech"
REALM="ams"

TOKEN=$(curl -s --location --request POST "$HOST/auth/realms/$REALM/protocol/openid-connect/token" \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data-urlencode 'grant_type=client_credentials' \
  --data-urlencode 'client_id=microservice-client' \
  --data-urlencode 'client_secret=9Yyq8DilKmhZmFeVc4NXi5CLgkWfYImJ' \
  --data-urlencode 'scopes=code' | jq -r '.access_token')

  echo -e "\033[0;33m ${TOKEN} \033[0m"

#use that access token to access a protected url
  curl  -s -H "Authorization: Bearer ${TOKEN}" "https://ams.saintrivers.tech/private/" | jq
 
