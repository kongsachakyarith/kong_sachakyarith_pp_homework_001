#for install git
sudo apt-get install git 
git --version

#for install curl
sudo apt install curl
curl --version

#for install jq
sudo apt install jq
jq --version

#for install docker
sudo apt install docker.io
docker --version

#for docker-compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version