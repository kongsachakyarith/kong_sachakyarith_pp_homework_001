# create two  categories
echo "---------------- 1. create two categories ------------------"
echo -n  "=> enter your name categories 1 : " 
read name
echo "----------------------------------"
echo -n  "=> enter your name categories 2 : " 
read names


curl  -X 'POST' \
  'https://ams.saintrivers.tech/api/v1/articles/categories' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d "{\"name\": \"${name}\"}" | jq

curl  -X 'POST' \
  'https://ams.saintrivers.tech/api/v1/articles/categories' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d "{\"name\": \"${names}\"}" | jq


#  create an article
echo "---------------- 2. create an article ------------------"
echo -n "=> enter your title : "
read title
echo -n "=> enter your body : "
read body
echo -n "=> enter your author : "
read author
echo -n "=> enter your id : "
read id


curl -X 'POST' \
  'https://ams.saintrivers.tech/api/v1/articles' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d "{
  \"content\": {
    \"title\": \"${title}\",
    \"body\": \"${body}\"
  },
  \"createdAt\": \"2022-08-06T06:55:44.492Z\",
  \"author\": \"${author}\",
  \"categories\": [
    $id
  ]
}" | jq

# add two comments to the article

echo "---------------- 3. create two commit ------------------"
echo -n "=> enter your caption 1 : "
read caption
echo -n "=> enter your commenter 1 : "
read commenter

echo "----------------------------------"

echo -n "=> enter your caption 2 : "
read caption_2
echo -n "=> enter your commenter 2 : "
read commenter_2

curl -X 'POST' \
  'https://ams.saintrivers.tech/api/v1/articles/e120574d-4d83-4a0b-bfb6-cc6b3511ed63/comments' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d "{
  \"caption\": \"${caption}\",
  \"commentedOn\": \"2022-08-06T08:23:15.004Z\",
  \"commenter\": \"${commenter}\"
}" | jq

curl -X 'POST' \
  'https://ams.saintrivers.tech/api/v1/articles/e120574d-4d83-4a0b-bfb6-cc6b3511ed63/comments' \
  -H 'accept: */*' \
  -H 'Content-Type: application/json' \
  -d "{
  \"caption\": \"${caption_2}\",
  \"commentedOn\": \"2022-08-06T08:23:15.004Z\",
  \"commenter\": \"${commenter_2}\"
}" | jq
